<?php

declare(strict_types=1);

namespace app\demo\controller;
use think\Controller;
use think\Db;
class Index 
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {

        echo '这是我用composer定义的类'."\r\n".'Run 起来了';
        // $model = Db::name('User');
        // $list = $model->where('id','<>',0)->select();
        // dump($list);
    }
}
