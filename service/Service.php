<?php

namespace demo\service;

class Service extends \ycwxTpCore\support\Service
{

    public function setVendorName(): void
    {
        $this->vendorName = 'ycwx_tp_app';
    }

    public function setVendorAppPath(): void
    {
        $this->vendorAppPath = realpath(dirname(__DIR__)) . DIRECTORY_SEPARATOR;
    }

    /**
     * 注册
     */
    public function register(): void
    {
        //注册配置
        $this->registerConfig();

        //注册绑定
        $this->registerBind();

        //注册命令行
        $this->registerCommand();

        //注册资源推送
        $this->registerPublishes();
    }


    /**
     * boot
     */
    public function boot(): void
    {
        //注册视图
        $this->bootView();

        //注册函数库
        $this->bootCommon();

        //注册路由
        $this->bootRouter();

        //注册多语
        $this->bootLang();

        //注册事件
        $this->bootEvent();

        //注册中间件
        $this->bootMiddleware();
    }



    /**
     * 视图
     * @return void
     */
    public function bootView()
    {
        //注册视图
        $this->loadViewsFrom();
    }

    public function bootLang()
    {
        //注册多语支持
        $this->loadLangsFrom();
    }

    public function bootEvent()
    {
        //注册事件
        $this->loadEventsFrom();
    }

    public function bootCommon()
    {
        //注册函数库
        $this->loadCommonFrom();
    }

    public function bootMiddleware()
    {
        //注册中间件配置
        $this->loadMiddlewareForm();
    }


}